﻿#nullable enable
using System;

namespace Learn.Entities
{
    public sealed class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Faculty { get; set; }
        public Course Course { get; set; }
        public int Group { get; set; }

        private Coursework _coursework;

        private bool Equals(Student other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Student) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Name, Surname, Faculty, Course, Group);
        }

        public static bool operator ==(Student left, Student right)
        {
            return Equals(right, left);
        }

        public static bool operator !=(Student left, Student right)
        {
            return !(left == right);
        }

        public override string ToString()
        {
            return $"{Name} {Surname} (id: {Id}), {Faculty}, {Course} course {Group} group";
        }
        
        public void TakeCoursework(Coursework work)
        {
            _coursework = work;
        }

        public void CompleteCoursework(Coursework work)
        {
            work.Status = true;
        }
    }
}
﻿using System;

namespace Learn.Entities
{
    public class Coursework
    {
        private string _name;
        private Student _student;
        private Teacher _mentor;
        private int _pagesCount;
        
        public bool Status { get; set; }

        public Coursework(string name, Student student, Teacher mentor, int pagesCount)
        {
            _name = name;
            _student = student;
            _mentor = mentor;
            _pagesCount = pagesCount;
            student.TakeCoursework(this);
            mentor.TakeCoursework(this);
        }

        private bool Equals(Coursework other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _name == other._name;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Coursework) obj);
        }
        
        public static bool operator ==(Coursework left, Coursework right)
        {
            return Equals(right, left);
        }

        public static bool operator !=(Coursework left, Coursework right)
        {
            return !(left == right);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_name, _student, _mentor, _pagesCount);
        }

        public override string ToString()
        {
            return
                $"Course work: {_name}.\n" +
                $"Author: {_student.Name}, {_student.Course} course,\n" +
                $"Mentor: {_mentor.Name}\n" +
                $"{_mentor.Chair} of {_mentor.Faculty}";
        }
    }
}
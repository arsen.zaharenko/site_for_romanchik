﻿namespace Learn.Entities
{
    public enum Course : byte
    {
        First = 1,
        Second = 2,
        Third = 3,
        Fourth = 4,
    }
}
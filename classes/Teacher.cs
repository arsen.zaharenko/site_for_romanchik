﻿using System;
using System.Collections.Generic;

namespace Learn.Entities
{
    public class Teacher
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Faculty { get; set; }
        public string Chair { get; set; }

        private List<Coursework> _courseworks = new List<Coursework>();

        private bool Equals(Teacher other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Teacher) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Name, Surname, Faculty, Chair);
        }

        public static bool operator ==(Teacher left, Teacher right)
        {
            return Equals(right, left);
        }

        public static bool operator !=(Teacher left, Teacher right)
        {
            return !(left == right);
        }

        public override string ToString()
        {
            return $"{Name} {Surname} (id: {Id}), {Faculty}, {Chair} chair";
        }

        public void TakeCoursework(Coursework work)
        {
            _courseworks.Add(work);
        }
    }
}
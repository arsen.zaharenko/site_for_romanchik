﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
namespace site_for_romanchik.Models
{
    public class Data
    {
        private static IConfiguration configuration;
        public List<Student> Students;
        public List<Mentor> Mentors;
        public List<Coursework> Courseworks;

        public Data()
        {
            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json", false, true);
            configuration = builder.Build();
            DbConnector db = new DbConnector(configuration);

            this.Students = db.GetStudents();
            this.Mentors = db.GetMentors();
            this.Courseworks = db.GetCourseworks();
        }

        public void UpdateData()
        {
            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json", false, true);
            configuration = builder.Build();
            DbConnector db = new DbConnector(configuration);

            this.Students = db.GetStudents();
            this.Mentors = db.GetMentors();
            this.Courseworks = db.GetCourseworks();
        }
        public List<ReadyCoursework> CreateCourseworksList()
        {
            var data = new Data();
            var readyCourseworks = new List<ReadyCoursework>();

            foreach (Coursework coursework in data.Courseworks)
            {
                string courseworkName = coursework._name;
                string status = coursework._status.ToString();

                int mentorId = coursework._mentorId;
                string mentorName = null;
                foreach (Mentor mentor in data.Mentors)
                {
                    if (mentor.Id == mentorId)
                    {
                        mentorName = mentor.Name;
                        break;
                    }
                }

                int studentId = coursework._studentId;
                string studentName = null;
                if (studentId != 0)
                {
                    foreach (Student student in data.Students)
                    {
                        if (student.Id == studentId)
                        {
                            studentName = student.Name;
                            break;
                        }
                    }
                }
                
                readyCourseworks.Add(new ReadyCoursework(mentorName, status, courseworkName, studentName));
            }

            return readyCourseworks;
        }

        public int AddCoursework(string mentorName, string status, string studentName, string courseworkName)
        {
            var id = CreateCourseworksList().Count + 1;
            var coursework = new ReadyCoursework(mentorName, status, courseworkName, studentName);

            var db = new DbConnector(configuration);
            return db.AddCoursework(coursework, id);
        }

        public int TakeCoursework(string mentorName, string studentName, string courseworkName)
        {
            var coursework = new ReadyCoursework(mentorName, "1", courseworkName, studentName);

            var db = new DbConnector(configuration);
            return db.TakeCoursework(coursework);
        }
    }
}

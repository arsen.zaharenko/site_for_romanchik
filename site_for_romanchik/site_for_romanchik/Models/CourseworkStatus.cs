﻿namespace site_for_romanchik.Models
{
    public enum CourseworkStatus
    {
        Untaken,
        Pending,
        Taken,
        Done
    } 
}

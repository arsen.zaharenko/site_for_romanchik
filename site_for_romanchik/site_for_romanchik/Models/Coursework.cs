﻿using site_for_romanchik.Models;
using System;

namespace site_for_romanchik.Models
{
    public class Coursework
    {
        public int _id { get; }
        public string _name { get; }
        public int _studentId { get; }
        public int _mentorId { get; }
        public int _pagesCount { get; }
        public CourseworkStatus _status { get; }

        public bool Status { get; set; }

        public Coursework(int id, string name, int studentId, int mentorId, int pagesCount, int status)
        {
            _id = id;
            _name = name;
            _studentId = studentId;
            _mentorId = mentorId;
            _pagesCount = pagesCount;
            _status = DefineStatus(status);
        }

        private CourseworkStatus DefineStatus(int status)
        {
            switch(status) {
                case 1:
                    return CourseworkStatus.Pending;
                case 2:
                    return CourseworkStatus.Taken;
                case 3:
                    return CourseworkStatus.Done;
                default:
                    return CourseworkStatus.Untaken;

            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace site_for_romanchik.Models
{
    public class ReadyCoursework
    {
        public string MentorName { get; }
        public string CourseworkStatus { get; }
        public string CourseworkName { get; }
        public string StudentName { get; }

        public ReadyCoursework(string mentorName, string status, string courseworkName, string studentName)
        {
            MentorName = mentorName;
            CourseworkStatus = status;
            CourseworkName = courseworkName;
            StudentName = studentName;
        }
    }
}

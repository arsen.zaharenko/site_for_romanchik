﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace site_for_romanchik.Models
{
    public class DbConnector
    {
        private string _connectionString;
        public DbConnector(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("CourseWorksConnection");
        }

        public List<Coursework> GetCourseworks()
        {
            var courseworks = new List<Coursework>();
            try
            {
                using var connnection = new SqlConnection(_connectionString);
                SqlCommand cmd = new SqlCommand("GetCourseworks", connnection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                connnection.Open();
                SqlDataReader read = cmd.ExecuteReader();
                while(read.Read())
                {
                    courseworks.Add(new Coursework(
                        Convert.ToInt32(read[0]),
                        read[4].ToString(),
                        Convert.ToInt32(read[1]),
                        Convert.ToInt32(read[2]),
                        Convert.ToInt32(read[3]),
                        Convert.ToByte(read[5]))
                    );
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return courseworks;
        }

        public List<Student> GetStudents()
        {
            var students = new List<Student>();
            try
            {
                using var connnection = new SqlConnection(_connectionString);
                SqlCommand cmd = new SqlCommand("GetStudents", connnection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                connnection.Open();
                SqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    students.Add(new Student(
                        Convert.ToInt32(read[0]),
                        read[1].ToString(),
                        Convert.ToInt32(read[3]),
                        Convert.ToInt32(read[4]),
                        Convert.ToInt32(read[2]),
                        Convert.ToInt32(read[5])
                        )
                    );
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return students;
        }

        public List<Mentor> GetMentors()
        {
            var mentors = new List<Mentor>();
            try
            {
                using var connnection = new SqlConnection(_connectionString);
                SqlCommand cmd = new SqlCommand("GetMentors", connnection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                connnection.Open();
                SqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    mentors.Add(new Mentor(
                        Convert.ToInt32(read[1]),
                        read[0].ToString(),
                        Convert.ToInt32(read[2])
                        )
                    );
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return mentors;
        }

        public int AddCoursework(ReadyCoursework coursework, int id)
        {
            using var connection = new SqlConnection(_connectionString);
            var command = new SqlCommand("AddCoursework", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@Id", id);
            command.Parameters.AddWithValue("@MentorName", coursework.MentorName);
            command.Parameters.AddWithValue("@CourseworkName", coursework.CourseworkName);
            command.Parameters.AddWithValue("@StudentName", coursework.StudentName);

            connection.Open();
            int res;
            try {
                res = command.ExecuteNonQuery();
            } catch (SqlException e) 
            { 
                return 0; 
            }
            connection.Close();
            Console.WriteLine("Data saved successfully");
            return res;
        }

        public int TakeCoursework(ReadyCoursework coursework)
        {
            using var connection = new SqlConnection(_connectionString);
            var command = new SqlCommand("TakeCoursework", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@MentorName", coursework.MentorName);
            command.Parameters.AddWithValue("@CourseworkName", coursework.CourseworkName);
            command.Parameters.AddWithValue("@StudentName", coursework.StudentName);

            connection.Open();
            int res;
            try
            {
                res = command.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                return 0;
            }
            connection.Close();
            Console.WriteLine("Data saved successfully");
            return res;
        }
    }
}

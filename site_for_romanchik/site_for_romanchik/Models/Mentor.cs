﻿using System;
using System.Collections.Generic;

namespace site_for_romanchik.Models
{
    public class Mentor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Chair { get; set; }

        public Mentor(int id, string name, int chair)
        {
            Id = id;
            Name = name;
            Chair = chair;
        }

        private bool Equals(Mentor other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Mentor) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Name, Chair);
        }

        public static bool operator ==(Mentor left, Mentor right)
        {
            return Equals(right, left);
        }

        public static bool operator !=(Mentor left, Mentor right)
        {
            return !(left == right);
        }

        public override string ToString()
        {
            return $"{Name} (id: {Id}), {Chair} chair";
        }
    }
}
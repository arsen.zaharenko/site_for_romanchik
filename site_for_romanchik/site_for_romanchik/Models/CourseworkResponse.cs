﻿using System.ComponentModel.DataAnnotations;

namespace site_for_romanchik.Models
{
    public class CourseworkResponse
    {
        [Required(ErrorMessage = "Please enter your Name")]
        public string StudentName { get; set; }

        [Required(ErrorMessage = "Please enter your Coursework Name")]
        public string CourseworkName { get; set; }

        [Required(ErrorMessage = "Please enter your Mentor Name")]
        public string MentorName { get; set; }

        public string IsTaken { get; set; }

    }
}
﻿using System;

namespace site_for_romanchik.Models
{
    public sealed class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Course { get; set; }
        public int Group { get; set; }
        public int courseworkId { get; set; }
        public int mentorId { get; set; }

        public Student(int courseworkId, string name, int course, int group, int id, int mentorId)
        {
            Id = id;
            Name = name;
            Course = course;
            Group = group;
            this.courseworkId = courseworkId;
            this.mentorId = mentorId;
        }
    
        private bool Equals(Student other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Student) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Name, Course, Group);
        }

        public static bool operator ==(Student left, Student right)
        {
            return Equals(right, left);
        }

        public static bool operator !=(Student left, Student right)
        {
            return !(left == right);
        }

        public override string ToString()
        {
            return $"{Name} (id: {Id}), {Course} course {Group} group";
        }
    }
}
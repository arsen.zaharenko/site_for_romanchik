﻿using Microsoft.AspNetCore.Mvc;
using site_for_romanchik.Models;

namespace site_for_romanchik.Controllers
{
    public class EducationController : Controller
    {
        private readonly StoreDbContext context;

        public EducationController(StoreDbContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public ViewResult Index()
        {
            return View();
        }

        [HttpPost]
        public ViewResult Index(CourseworkResponse courseResponse, Data data)
        {
            if (ModelState.IsValid) {
                if (data.AddCoursework(courseResponse.MentorName, "Pending", courseResponse.StudentName, courseResponse.CourseworkName) == 0) 
                {
                    ModelState.AddModelError("StudentName", "DATA NOT FOUND IN DB");
                }
            }
            ModelState.Clear();
            return View();
        }

        public ViewResult Take(CourseworkResponse courseResponse, Data data)
        {
            if (ModelState.IsValid)
            {
                ModelState.Clear();           
                if (data.TakeCoursework(courseResponse.MentorName, courseResponse.StudentName, courseResponse.CourseworkName) == 0)
                {
                    ModelState.AddModelError("IsTaken", "STUDENT NOT FOUND IN DB");
                }
                return View();
            }
            ModelState.Clear();
            ModelState.AddModelError("IsTaken", "STUDENT NOT FOUND IN DB");
            return View("Index");
        }
    }
}
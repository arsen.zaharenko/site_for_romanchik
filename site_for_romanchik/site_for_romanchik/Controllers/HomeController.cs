﻿using Microsoft.AspNetCore.Mvc;

namespace site_for_romanchik.Controllers
{
    public class HomeController : Controller
    {
        public ViewResult Index()
        {
            return View();
        }
    }
}
